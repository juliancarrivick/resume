#!/bin/sh

get_gitlab_link() {
    local link_base="https://gitlab.com/juliancarrivick/resume/-/tree"

    if [ -n "$CI_COMMIT_TAG" ]; then
        echo "$link_base/$CI_COMMIT_TAG"
    elif [ -n "$CI_COMMIT_SHA" ]; then
        echo "$link_base/$CI_COMMIT_SHA"
    else
        echo "$link_base/$(git rev-parse HEAD)"
    fi
}

get_commit_date() {
    commit_date=$(git log -1 --format="%ci" | awk '{print $1}')
    year=$(date -d "$commit_date" +"%Y")
    month=$(date -d "$commit_date" +"%-m")
    day=$(date -d "$commit_date" +"%-d")

    echo "{\"year\": $year, \"month\": $month, \"day\": $day}"
}

output_file="inputs.json"
echo "{" > $output_file
echo "  \"source-link\": \"$(get_gitlab_link)\"," >> $output_file
echo "  \"modified-date\": $(get_commit_date)" >> $output_file
echo "}" >> $output_file

echo "JSON file generated: $output_file"
cat $output_file
