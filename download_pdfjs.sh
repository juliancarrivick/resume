#!/bin/bash

version="4.0.189"
url="https://github.com/mozilla/pdf.js/releases/download/v$version/pdfjs-$version-dist.zip"

destination="public/"

wget "$url" -O "$destination/tmp.zip"
unzip -q -d "$destination/pdfjs" "$destination/tmp.zip"
rm -f "$destination/tmp.zip"
