#import "_helpers.typ": underlined-link

#set cite(form: "prose")

== Recent Roles & Experience
#v(8pt, weak: true)

#include "roles/2024-icrar.typ"

#include "roles/2022-smrg-uwa.typ"
