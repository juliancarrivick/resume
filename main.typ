#import "_helpers.typ": last-updated

#set document(
  title: "Julian Carrivick - Resume",
  date: last-updated
)

#set page(
  paper: "a4",
  margin: (
    left: 1.8cm,
    right: 1.8cm,
    top: 1.4cm,
    bottom: 1.4cm
  ),
)
#set page(footer: context {
  if counter(page).get().first() > 1 [
    #include "footer.typ"
  ]
})

#set text(font: "Source Sans 3", size: 11pt)
#set underline(offset: 1pt)
#show heading.where(level: 4): it => text(weight: "bold", style: "italic", it.body)
#set par(justify: true)

#include "header.typ"
#include "about.typ"
#include "recent-roles-and-experience.typ"

#pagebreak()

#include "other-roles-and-experience.typ"

#align(bottom, include "publications.typ")
#v(1cm)
