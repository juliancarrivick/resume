#import "_helpers.typ": icon, source-link, last-updated

#grid(
    columns: (1fr, 1fr),
    align(left, [
        #icon("gitlab", "GitLab")
        #link(source-link)[juliancarrivick/resume]
    ]),
    align(right, [Last updated #last-updated.display()])
)
