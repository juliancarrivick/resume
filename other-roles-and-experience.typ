#import "_helpers.typ": icon

#include "roles/2021-adapt.typ"

#include "roles/2016-ibm.typ"

== Education
_#link("https://www.curtin.edu.au/")[Curtin University]: 2012 - 2015_
- Bachelor of Engineering (Software) with First Class Honours
- Course Weighted Average of 87.44

== Other Experience

#grid(
  columns: (1fr, auto),
  align: (left, right),
  [=== #link("https://orchestrate.community")[Orchestrate] - Community Music Management Platform],
  [#icon("gitlab", "Gitlab") #link("https://gitlab.com/maestro-software/orchestrate-platform")[maestro-software/orchestrate-platform]],
)
#v(4pt, weak: true)
_Founder, Product Owner and Developer: 2015 - current_

- Define requirements in conjunction with customers, design and implement corresponding features, obtain feedback prior to release
- Deliver major features such as automated billing, mailing lists and performance management
- Develop with industry standard tools: #link("https://www.postgresql.org")[PostgreSQL], #link("https://dotnet.microsoft.com/en-us/learn/aspnet/what-is-aspnet-core")[ASP.NET Core], and #link("https://angular.io/")[Angular], deploy to #link("https://fly.io/")[Fly.io]
- Automated testing and deployment using GitLab Continuous Integration

=== Personal Homelab

- Manage #link("https://k3s.io/")[K3s] Kubernetes installation on #link("https://radxa.com/products/rock5/5b/")[Raxda Rock 5B] single board computer
- Self-host personal services such as #link("https://immich.app")[Immich] (photo management), #link("https://nextcloud")[Nextcloud] (file hosting), #link("https://atuin")[Atuin] (shell history synchronisation)
- Securely connect using #link("https://tailscale.com/")[Tailscale] point-to-point Wireguard VPN
- Network management using #link("https://store.ui.com/us/en/collections/uisp-wired-advanced-routing-compact-poe/products/er-x")[EdgeRouter X], and Ubiquiti #link("https://ui.com/uk/en/wifi")[UniFi Access Point]
