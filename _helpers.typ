#let icon(name, alt) = {
  box(
    baseline: 0.25em,
    height: 1em,
    image(
      "icons/" + name + ".svg",
      alt: alt
    ),
  )
}

#let icon_link(name, url, alt) = {
  link(url)[
    #icon(name, alt)
  ]
}

#let underlined-link(body, url) = {
  show link:underline
  link(body, url)
}

#let invisible(content) = {
  h(0pt, weak: true)
  box(width: 0pt, height: 0pt, clip: true)[#content]
}

#let inputs = json("inputs.json")
#let source-link = inputs.source-link
#let last-updated = datetime(
  year: inputs.modified-date.year,
  month: inputs.modified-date.month,
  day: inputs.modified-date.day
)
