#import "../_helpers.typ": underlined-link, invisible

=== #link("https://www.icrar.org/")[International Center for Radio Astronomy Research]
_Software Research Engineer: September 2022 - March 2024_

- Collaborate globally on the #link("https://www.skao.int")[Square Kilometre Array] radio telescope using #link("https://scaledagile.com/what-is-safe/")[Scaled Agile Framework]:
  - Develop and optimise and test Python real-time data-processing pipelines with low-latency & high-throughput requirements
  - Identify and resolve bottlenecks in source code of underlying C & C++ libraries e.g.
    #link("http://casacore.github.io/casacore/")[Casacore] astronomy toolkit,
    #link("https://docs.xarray.dev/en/stable/index.html")[Xarray]
    and #link("https://numpy.org/")[NumPy]
  - Maintain component Docker images and Helm charts, develop system integration tests on Kubernetes
  - Cultivate understanding of radio astronomy & interferometry
- Support scientific endeavours of the institute, primarily on:
  - #link("https://github.com/ICRAR/daliuge")[DALiuGE]: A workflow orchestrator written in Python and deployed to super computers, use Python #link("https://docs.python.org/3/library/pickle.html")[pickle] library to execute jobs on remote nodes
  - #link("https://github.com/ICRAR/shark")[shark]: A galaxy formation model written in #box[C++11], utilising #link("https://www.boost.org/")[Boost], #link("https://www.openmp.org/")[OpenMP] and #link("https://www.hdfgroup.org/solutions/hdf5/")[HDF5]
  - Discuss developmental Machine Learning models running on GPU hardware (e.g. galaxy classification)
- Assist with outreach activities such as #link("https://www.icrar.org/events/astrofest-2023/")[Astrofest] and #link("https://www.icrar.org/outreach-education/work-experience/")[work experience students]

==== Achievements

- Lead discussions with stakeholders, design and implement telescope calibration  loop running across #link("https://www.tango-controls.org/")[Tango] control system, #link("https://spead2.readthedocs.io/en/latest/")[SPEAD2] network protocol and #link("https://kafka.apache.org/")[Apache Kafka], demonstrating final solution
- Design and review of sophisticated time and frequency aggregation enhancement, for a thirty-fold improvement in throughput
