=== #link("https://www.ibm.com")[IBM]
_Software Engineer: January - November 2016_

- Develop component in Java to ingest CSV and build a binary MaxMind IP Geolocation file, test implementation using #link("https://junit.org/junit5/")[JUnit]
- Write bug fixes and tests in Python and Go for open-source Blockchain application: #link("https://github.com/hyperledger/fabric/commits/main?author=cjulian@au1.ibm.com")[Hyperledger Fabric], support deployments onto IBM Cloud
- Provide L3 Support to Customers #sym.dash troubleshoot and diagnose over the phone, prepare and package fixes for customers
