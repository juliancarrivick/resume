#import "../_helpers.typ": underlined-link

=== Simulation Modelling Research Group \ University of Western Australia
_Software Developer: February 2021 - September 2022_

- Lead efforts to scope, design, document and implement features for #box[C++20] COVID-19 model, ensuring compatibility across #link("https://visualstudio.microsoft.com/vs/features/cplusplus/")[MSVC], #link("https://gcc.gnu.org/")[GCC] and #link("https://clang.llvm.org/")[Clang]
- Proactively profile and implement performance improvements to model: total of 25x reduction in run times by parallelising and profiling hot-paths & critical data structures (\~20min simulation reduced to \~50sec)
- Introduce industry best practices to development: static analysis (Clang Format & Tidy) and automated testing using `Boost.Test` continuously integrating on Github Actions
- Output simulation results to #link("https://www.sqlite.org/")[SQLite] database, aggregating and analysing using Python & #link("https://pandas.pydata.org/")[Pandas]

==== Achievements

- Implement complex model enhancements to simulate observed COVID-19 vaccine effectiveness, including booster shots and waning immunity
- Design, conduct, interpret and present COVID-19 modelling results to the Western Australian Department of Health, publishing the outcomes: @Milne2022.03.09.22272170, @Milne2022
