=== #link("https://adaptbydesign.com.au/")[ADAPT by Design]
_Software Engineer: November 2016 - February 2021_

- Design, implement and test features for the ADAPT platform using #link("https://dotnet.microsoft.com/")[C\# & .NET], #link("https://www.typescriptlang.org/")[Typescript] & #link("https://angular.io/")[Angular] and #link("https://www.microsoft.com/en-gb/sql-server/")[SQL Server], deploy to #link("https://azure.microsoft.com/")[Azure] cloud
- Write unit tests using MSTest & Jasmine and integration tests in Cypress
- Develop using Agile methodology, manage work in #link("https://www.atlassian.com/software/jira")[JIRA], document architecture and designs in #link("https://www.atlassian.com/software/confluence")[Confluence]
- Write communications and run workshops with customers
- Maintain internal network infrastructure and services such as #link("https://www.pfsense.org/")[pfSense] router/firewall, #link("https://www.vmware.com/au/products/esxi-and-esx.html")[ESXi] server and virtual machines hosting test environments
