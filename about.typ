#import "_helpers.typ": icon, icon_link, underlined-link

#let about = [
== About
I am strong collaborator and voracious learner who delights in solving tough problems in a team to produce high-quality products. I pride myself on my ability to communicate well with people of all backgrounds and my application to any task set of me.

I have a keen interest in writing maintainable code which has to consider performance requirements or resource constraints. I particularly enjoy learning the science and mathematics of the domain I am working in and applying it to real-world problems.
]

#let languages-and-tools = [
== Languages & Tools
#[
    #set text(2em)
    #show par: set block(spacing: 0em)

    #icon_link("c-plus-plus", "https://cplusplus.com/", "Python")
    #icon_link("python", "https://www.python.org/", "C++")
    #icon_link("c-sharp", "https://dotnet.microsoft.com/languages/csharp", "C#")
    #icon_link("typescript", "https://www.typescriptlang.org/", "TypeScript")
    #icon_link("java", "https://www.java.com/", "Java")
    #icon_link("go-gopher", "https://go.dev", "Go")

    #icon_link("angular", "https://angular.io/", "Angular Framework")
    #icon_link("dotnet", "https://dotnet.microsoft.com", ".NET Core")

    #icon_link("kubernetes", "https://kubernetes.io/", "Kubernetes")
    #icon_link("helm", "https:/helm.sh", "Helm")
    #icon_link("docker", "https://www.docker.com", "Docker")
    #icon_link("postgresql", "https://www.postgresql.org", "PostgreSQL")
    #icon_link("sqlite", "https://www.sqlite.org/", "SQLite")

    #v(0.1cm)
    #h(0.1cm)
    #icon_link("kafka", "https://kafka.apache.org/", "Kafka")
    #text(0.75em)[#icon_link("hdf", "https://www.hdfgroup.org/solutions/hdf5/", "HDF5")]
    #h(0.1cm)
    #icon_link("xarray", "https://docs.xarray.dev/", "Xarray")
]
]

#grid(
    columns: (1fr, auto),
    column-gutter: 1cm,
    about, languages-and-tools
)
