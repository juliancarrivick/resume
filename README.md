# My Resume

This is a living document, changing as my career progresses. Important milestones will be tagged.

Written in [Typst](https://github.com/typst/typst). Layout inspired by [Typst-CV-Resume](https://github.com/jxpeng98/Typst-CV-Resume).

Icons taken from [SVGRepo](https://www.svgrepo.com/).
