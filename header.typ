#import "_helpers.typ": icon

#let links = (
  (icon: "email", alt: "Email", link: "mailto:julian@carrivick.com"),
  (icon: "linkedin", alt: "LinkedIn profile", link: "https://www.linkedin.com/in/julian-carrivick/", display: "julian-carrivick"),
  (icon: "gitlab", alt: "GitLab profile", link: "https://gitlab.com/juliancarrivick", display: "juliancarrivick"),
  (icon: "github", alt: "GitHub profile", link: "https://github.com/juliancarrivick", display: "juliancarrivick"),
)

#let links-row(linkArray) = {
  set text(1em)

  linkArray.map(l => {
    icon(l.icon, l.alt)
    h(0.25em)

    if "display" in l.keys() {
      link(l.link)[#{l.display}]
    } else {
      link(l.link)
    }
  }).join(h(1em))
}

= Julian Carrivick
#set text(size: 11.5pt)
#links-row(links)
